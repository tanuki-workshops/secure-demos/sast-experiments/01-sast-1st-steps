package example;

public class Main {

    public static void main(String[] args) throws URISyntaxException, IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
      System.out.println("Hello");
    }

    String generateSecretToken() {
        Random r = new Random();
        return Long.toHexString(r.nextLong());
    }
}

